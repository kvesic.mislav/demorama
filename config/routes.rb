Rails.application.routes.draw do
	root to: "pages#index"

	get "first", to: "pages#first"
	get "second", to: "pages#second"
	get "third", to: "pages#third"
end
